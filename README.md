# Codes for the UGA Open research data monitor

View contextualized results on the website : [mlarrieu.gricad-pages.univ-grenoble-alpes.fr/open-research-data-monitor](https://mlarrieu.gricad-pages.univ-grenoble-alpes.fr/open-research-data-monitor)

<br />
<br />

## Sources & identification methods

### Recherche Data Gouv

- Recherche en format texte de `UGA` et `grenoble AND alpes` dans les champs suivants : `author`, `contributor`, `datasetContactAffiliation`, `producerAffiliation`



### DataCite

- recherche avec les clients Datacite de l'UGA : `inist.osug`, `client.uid:inist.sshade`, `client.uid:inist.resif`, `client_id:inist.persyval`

- avec les ROR de l'université sur les champs `creators` et `contributors`

- en format texte `grenoble AND alpes` sur le champs `publisher`

- instruire l'UGA comme financeur


### Zenodo

- recherche en format texte `"(\"grenoble alpes\" OR \"grenoble alps\" OR \"grenoble INP\" OR \"polytechnique de grenoble\" OR \"Grenoble Institute of Technology\" OR \"univeristé de grenoble\" )"` sur les champs `author` et `contributor`

- veille sur l'API car demain il devrait être possible de requêter par ROR ?

### Nakala

- recherche par les déposants relevant de l'UGA. Liste obtenues via HumaNum et enrichie manuellement

- instruire côté `dcterms:publisher`

### Barometre de la science ouverte UGA

- a faire annuellement à chaque MAJ du jeux de données
- récupérer la liste de publications, filter sur celles où des jeux de données ont été produits
- passer par HAL pour retrouver les DOI de ces jeux de données (champs `researchData_s`)

<br />
<br />

## Filters
- we removethe following datacite types `["Book", "ConferencePaper", "ConferenceProceeding", "JournalArticle", "BookChapter", "Service", "Preprint"]`
- we remove the following datacite clients `["rg.rg", "inist.epure"]`



<br />
<br />

## Credits

* Élias Chetouane: collecting data, program automation
* Maxence Larrieu: collecting data, enrichment & visualisation

as members of GRICAD & CDGA
