import pandas as pd, matplotlib, matplotlib.pyplot  as plt
import z_my_functions as my_fct
from datetime import datetime, timezone



temp_date = pd.to_datetime('2024-02-01', format='%Y-%m-%d')

df = my_fct.load_and_treat_csv()

# sort df to the most recent
df.sort_values(by = "registered", ascending = False, inplace = True, ignore_index = True )
print(df.columns)

## transform to date format
df["registered_date"] = pd.to_datetime( df["registered"], errors='coerce')

## age of deposit is current time - date of registration
# df["age_of_deposit_days"] = datetime.now(timezone.utc) - df["registered_date"] ## this give a full time difference
df["age_of_deposit_days"] = (datetime.now(timezone.utc) - df['registered_date']).dt.total_seconds() / 60 / 60 / 24


# depot sur les 30 derniers jours
df_last_weeks = df[ df["age_of_deposit_days"] <= 30]


df_graph = pd.crosstab(df_last_weeks["registered"].str[5:10], df_last_weeks["client"])
df_graph.index.rename("Day of registration", inplace = True)

## ______0______ produce graphs

## a set of color via plt
### see color palett https://matplotlib.org/stable/users/explain/colors/colormaps.html
colors = [plt.cm.Set3(i) for i in range(len(df_graph.columns))]

ax = df_graph.plot(
    kind = "bar", 
    figsize = (10, 7),
    stacked = True,
    width = 0.3,
    color = colors,
    rot = 80
    )


## _______ configurer l'afichage
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.set_ylabel("Number of datasets", labelpad = 2)
ax.yaxis.grid(ls=":", alpha=0.2)
ax.tick_params(axis='both', which='major', labelsize=8)
# plt.xlabel(None)

plt.legend(reverse = False)

plt.title(f"Research data registered over the last 30 days\ndistributed by DataCite client",
	fontsize = 18, x = 0.5, y = 1.03, alpha = 0.8)
plt.suptitle(f"n = {len(df_last_weeks)}", fontsize = 12, x = 0.5, y = 0.89, alpha = 0.6)

plt.savefig("hist-last-datasets-by-client.png")