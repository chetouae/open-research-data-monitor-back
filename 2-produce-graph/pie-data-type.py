import pandas as pd, matplotlib, matplotlib.pyplot  as plt
import z_my_functions as my_fct
import random

df = my_fct.load_and_treat_csv()
#print(df.columns)

df_type = df["resourceTypeGeneral"].value_counts()
# print(df_type_raw)

# ## regroup small values in "other"
# treshold = 20
# df_type = df_type_raw[df_type_raw > treshold]
# df_type["other"] = df_type[df_type <= treshold].sum()



#define a color palette to use
### see color palett https://matplotlib.org/stable/users/explain/colors/colormaps.html
colors = [plt.cm.Set3(i) for i in range(len(df_type))]
random.shuffle(colors) ## so that blue is not more the first item

plt.subplots(figsize=(10, 7))
# plt.subplots_adjust(1, 0.01 , 0.9, 0.2) # pour modifier la taille du pie 
plt.pie(df_type, colors = colors, autopct=lambda p: '{:.0f}%'.format(round(p)) if p > 1 else '', startangle = 160)
## auto pct only if value > 1

plt.legend(df_type.index, loc = (0.80, 0.2), framealpha = 0.95)

plt.title(f"Type of research data", fontsize = 20, x = 0.5, y = 1.03, alpha = 0.6)
plt.suptitle(f"n = {len(df)}", fontsize = 11, x = 0.5, y = 0.87, alpha = 0.6)
plt.tight_layout(h_pad = 0)
plt.savefig("pie--datacite-type.png")
print(f"\ngraph produced pie--datacite-type.png")

# plt.show()
