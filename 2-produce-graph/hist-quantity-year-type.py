import pandas as pd, matplotlib, matplotlib.pyplot  as plt
import z_my_functions as my_fct
from matplotlib import colormaps


df = my_fct.load_and_treat_csv()

print(df.columns)

## create year from registration
df["year"] =  df["registered"].str[:4]

df_year_type = pd.crosstab( df["year"], df["resourceTypeGeneral"])
df_year_type.index.rename("year", inplace = True)

## _______ produce graphs

## a set of color via plt
### see color palett https://matplotlib.org/stable/users/explain/colors/colormaps.html
colors = [plt.cm.Set3(i) for i in range(len(df_year_type.columns))]

ax = df_year_type.plot(
    kind = "bar", 
    figsize = (10, 7),
    stacked = True,
    width = 0.7,
    color = colors,
    rot = -0,
    fontsize = 14
    )

## _______ configurer l'afichage
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.set_ylabel("Number of datasets", labelpad = 10)
ax.yaxis.grid(ls=":", alpha=0.5)
ax.tick_params(axis='both', which='major', labelsize=10)
plt.xlabel(None)

plt.legend(loc="center", reverse = True, bbox_to_anchor=(0.45, 0.65), fontsize = 10)

plt.title(f"Distribution of research data by registration year\nand DataCite types",
	fontsize = 18, x = 0.5, y = 1.03, alpha = 0.8)
plt.suptitle(f"n = {len(df)}", fontsize = 12, x = 0.5, y = 0.90, alpha = 0.6)

plt.savefig("hist-quantity-year-type.png")
