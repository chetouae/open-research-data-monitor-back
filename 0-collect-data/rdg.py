# Récupérer les DOIs de l'UGA depuis Rechrche Data Gouv
## 2023-12-01, Elias Chetouane

"""
## Documentation
* Noms des champs à requêter : https://entrepot.recherche.data.gouv.fr/api/metadatablocks/citation
* Doc API Dataverse : https://guides.dataverse.org/en/5.12.1/api/search.html

## Remarques
- Problème : certaines affiliations renseignent la ville, mais le labo n'est pas affilié à l'UGA. Cela les fait apparaitre alors qu'il ne faudrait pas les prendre en compte...
- Obligation de cherche Greoble et UGA car champ libre, donc parfois l'affiliation à l'UGA est renseignée par UGA et parfois Univ Greoble Alpes
- Adapter une fois mise en place du ROR
"""

import requests

# requetes de base sur chaque champ qui nous intéresse : affiliation du contact, auteurs, producteur et contributeurs
# dans chaque champ, recherche "UGA" ou "Grenoble"
urls = [
    'https://entrepot.recherche.data.gouv.fr/api/search?q=*&fq=datasetContactAffiliation%3AUGA',
    'https://entrepot.recherche.data.gouv.fr/api/search?q=*&fq=authorAffiliation%3AUGA',
    'https://entrepot.recherche.data.gouv.fr/api/search?q=*&fq=producerAffiliation%3AUGA',
    'https://entrepot.recherche.data.gouv.fr/api/search?q=*&fq=contributorAffiliation%3AUGA',
    'https://entrepot.recherche.data.gouv.fr/api/search?q=*&fq=datasetContactAffiliation%3A(Grenoble AND Alpes)',
    'https://entrepot.recherche.data.gouv.fr/api/search?q=*&fq=authorAffiliation%3AGrenoble(Grenoble AND Alpes)',
    'https://entrepot.recherche.data.gouv.fr/api/search?q=*&fq=producerAffiliation%3AGrenoble(Grenoble AND Alpes)',
    'https://entrepot.recherche.data.gouv.fr/api/search?q=*&fq=contributorAffiliation%3AGrenoble(Grenoble AND Alpes)'
    # possiblilité d'ajouter d'autres requêtes
]

# on définit une fonction pour la lancer la requete avec chaque url pour les différentes affiliations
def get_results(url):
    req = requests.get(url)
    #print(req.url)
    results = [req.json()]
    
    # obtenir les résultats de chaque page dans la liste results
    nb_res = results[0]["data"]["count_in_response"]
    count = nb_res
    page = 1
    while(nb_res > 0):
        newurl = url+"&start="+str(count)
        req = requests.get(newurl)
        results.append(req.json())
        nb_res = results[page]["data"]["count_in_response"]
        count += nb_res
        page += 1
    return results

# on crée une fonction pour ajouter les DOIs dans une liste

def get_dois(results):
    dois = []
    nb_dois = 0

    # prendre en compte les résultats de chaque page
    for res in results:
        num_dois = res["data"]["items"]
        nb_dois += len(num_dois)

        for item in num_dois :
            dois.append(item.get("global_id"))
        
    print("\tnb DOIs\t\t" + str(nb_dois))
    return dois

print("\n\nRunning rdg.py")

# on récupère les dois
dois = []

for url in urls:
    dois += get_dois(get_results(url))

# on supprime les doublons

unique_dois = list(set(dois))

print("\tnb DOIs uniques\t\t" + str(len(unique_dois)))

# exporter la liste de DOI au format txt

with open("rdg-dois.txt", 'w') as f :
    # memo [4:] pour retirer "doi:" au début de chaque lignes
    [f.write(f"{line[4:]}\n") for line in unique_dois]

