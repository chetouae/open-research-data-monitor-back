# Récupérer les RORs de l'env. UGA
## 2024-01-08, Maxence Larrieu


"""
## step
- recupere les ROR qui ont pour parent le ROR de l'UGA
- HAL : on réduit aux structures VALID
- ROR apporte du bruit et demande un nettoyage manuel a posteriori
- ROR : on retire les UMR passées et les réseaux nationaux type GDR, FR, IR
- cette dernière étape se fait à la main à partir des tableaux produits


## documentation

* HAL
https://api.archives-ouvertes.fr/ref/structure/?q=parentRor_id:02rx3b187


* ROR 
https://ror.readme.io/docs/ror-schema-api-v2-beta
https://api.dev.ror.org/v2/organizations/02rx3b187
"""


import requests, json, pandas as pd
import datetime


## ___0____ from HAL structure API get UGA ROR childs

def hal_struct_return_docs(ror_id) : 
	"""
	HAL structure referentiel
	https://api.archives-ouvertes.fr/docs/ref/?resource=structure&schema=fields#fields
	list structures who have a parent corresponding to ror_id
	output raw results inside the "docs" key
	"""

	# query human language : childs of ROR structure that are VALID and has a ROR id
	req_filter = f"q=parentRor_id:{ror_id}&fq=ror_s:[%22%22%20TO%20*]\
	&fq=valid_s:VALID&fl=ror_s,name_s,type_s,docid"

	req = requests.get("https://api.archives-ouvertes.fr/ref/structure/?" + req_filter + "&rows=500")
	# to debug print(req.url)
	res = req.json()
	return res["response"]["docs"]


def hal_struct_return_ror_childs(ror_id) :
	"""
	boucle récursive
	à partir d'un résultat de HAL structure sur parent_ror
	itérer sur chacun des éléments et les ajouter
	et refaire pareil sur ces éléments
	"""	
	hal_results = hal_struct_return_docs(ror_id)

	for item in hal_results : 
		# in HAL structure API, ror are in list
		for ror_url in item["ror_s"] :

			## find number of ROR child to the sub element
			ror_id =  convert_ror_url_to_id(ror_url)
			new_hal_results = hal_struct_return_docs(ror_id)
			nb_childs = len(new_hal_results)
			struct_info = [ror_url, item["name_s"], item["type_s"], item["docid"], nb_childs]
			
			ror_childs.append(struct_info)

			## if sub element has ROR child lets iterate on these new results
			if nb_childs > 0 :
				new_ror_id = convert_ror_url_to_id(ror_url)
				hal_struct_return_ror_childs(new_ror_id)


def convert_ror_url_to_id (ror_url) : 
	# convert https://ror.org/05rwrfh97 to 05rwrfh97
	## car l'API de HAL renoie l'URL pour ces deux champs ror_s et ror_urls :/ 
	return ror_url[ ror_url.rfind("/") +1 :]


ror_childs = []
hal_struct_return_ror_childs("02rx3b187")

print(f"from HAL, nb of childs raw\t{len(ror_childs)}")

df_hal = pd.DataFrame(ror_childs, columns = ["ror", "name", "type", "docid", "hal_child_nb"])
df_hal.drop_duplicates(subset = ["ror"], inplace = True)
print(f"from HAL, nb of unique childs\t{len(df_hal)}")


## ____2____  from ROR API get UGA ROR childs

def ror_get_child_info(ror_id) : 
	req = requests.get("https://api.dev.ror.org/v2/organizations/" + ror_id)
	res = req.json()
	types_list = [ item for item in res["types"]]
	org_type = ",".join(types_list)
	return {
		"org_type" : org_type,
		"relation_nb" : len(res["relationships"])
		}

# query on ROR w UGA ROR_id
req = requests.get("https://api.dev.ror.org/v2/organizations/02rx3b187")
res = req.json()
# relationships contains all ror "childs"
ror_list = res["relationships"]
print(f"from ROR, nb of childs finded\t{len(ror_list)}")

## for each ROR finded enrich with ROR API
data_from_rors = []
for item in ror_list :
	if item["id"] not in df_hal.ror.values : 
		org_info = ror_get_child_info(item["id"])
		data_from_rors.append( 
			[ item["id"], item["label"], org_info["org_type"], org_info["relation_nb"] ]
		)


df_ror = pd.DataFrame(data_from_rors, columns = ["ror", "name", "type", "ror_relation_nb"])

df = pd.concat([df_hal, df_ror], ignore_index = True)
date = datetime.datetime.now().strftime("%Y-%m-%d") ## format date has 2023-12-29
df.to_csv(f"UGA-ror-childs--{date}.csv", index = False)


print("nb of RORs finded", len(df))
